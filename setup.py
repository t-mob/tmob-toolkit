#!/usr/bin/env python
# coding: utf-8

from setuptools import setup

setup(
    name='tmob-toolkit',
    version='1.0.0',
    description='T-Mob Toolkit',
    author='Ignacio Juan Martín Benedetti',
    author_email='ignacio.benedetti@t-mobs.com',
    packages=['tmob_toolkit'],
    install_requires=[],
)
