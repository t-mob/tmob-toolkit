# coding: utf-8


def field_validator(data, field, validator=None):
    """
       Recibe un Diccionario, un nombre de campo y, opcionalmente, una función
    para validar el valor de dicho campo.

       Devuelve un valor y un error. Si el valor cumple la validación,
    error es False. Si el valor no cumple la validación entonces error es True.
    """

    field_value = data.get(field)

    if validator is None:
        if field_value is not None:
            return field_value, False

    elif callable(validator):
        if validator(field_value) is True:
            return field_value, False

    else:
        raise Exception(
            'validator "{}" is not callable'.format(type(validator))
        )

    return None, True
