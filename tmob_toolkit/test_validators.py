

from .validators import field_validator


def test_field_validator_field_missing():
    data = {}

    value, error = field_validator(data, 'field')

    assert value is None
    assert error is True


def test_field_validator_field_none():
    data = {'field': None}

    value, error = field_validator(data, 'field')

    assert value is None
    assert error is True


def test_field_validator_field_ok():
    data = {'field': 1}

    value, error = field_validator(data, 'field')

    assert value is 1
    assert error is False


def test_field_validator_field_validator_ok():
    data = {'field': 1}

    value, error = field_validator(data, 'field', lambda x: isinstance(x, int))

    assert value is 1
    assert error is False


def test_field_validator_field_validator_fail():
    data = {'field': 'string'}

    value, error = field_validator(data, 'field', lambda x: isinstance(x, int))

    assert value is None
    assert error is True
